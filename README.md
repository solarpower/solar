# README #

This is a C# program that calculates the Solar power radiation during sunrise and sunset.

The code first calculates the declination of the sun, cosine of the sun position, sinus of the sun position, and the difference between the solar and the actual time. All these parameters are used to calculate the sunrise and sunset times.

The calculated value is used to determine the solar power required to power security lights. These lights are also PIR motion sensor security enabled lights. They have batteries onboard. Hence it is important to understand the power wattage required to charge efficiently.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact